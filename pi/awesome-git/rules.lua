-- Load dependencies
local awful = require("awful")
local beautiful = require("beautiful")
local ruled = require("ruled")

-- Get dpi function from xresources
local dpi = require("beautiful.xresources").apply_dpi


-- Rules to apply to new clients.
ruled.client.connect_signal("request::rules", function()
    -- All clients will match this rule.
    ruled.client.append_rule {
        id         = "global",
        rule       = { },
        properties = {
            focus     = awful.client.focus.filter,
            raise     = true,
            screen    = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    }

    -- Floating clients.
    ruled.client.append_rule {
        id       = "floating",
        rule_any = {
            instance = { "copyq", "pinentry" },
            class    = {
                "Arandr", "Blueman-manager", "Gpick", "Kruler", "Sxiv", "Steam",
                "Tor Browser", "Wpa_gui", "veromix", "xtightvncviewer"
            },
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name    = {
                "Event Tester",  -- xev.
            },
            role    = {
                "AlarmWindow",    -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",         -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        except_any  = {
            name    = {
                "Steam",  -- Main Steam client window
            },
        },
        properties = { floating = true }
    }

    -- Add titlebars to normal clients and dialogs
    ruled.client.append_rule {
        id         = "titlebars",
        rule_any   = { type = { "normal", "dialog" } },
        properties = { titlebars_enabled = false     }
    }

    -- Floating and Focused
    ruled.client.append_rule {
        id         = "focused-floaating",
        rule_any   = {
            class = {
                "KeePassXC", "Nextcloud"
            },
        },
        properties = { focus = true, floating = true }
    }

    -- Titlebars Enabled
    ruled.client.append_rule {
        id         = "titlebars",
        rule_any   = {
            class = {
                "xournalpp", "com.github.xournalpp.xournalpp"
            }
        },
        properties = { titlebars_enabled = true }
    }

    -- Application-specific Rules
    ruled.client.append_rule {
        rule       = { class = "discord"     },
        properties = { screen = 2, tag = "1" }
    }

    ruled.client.append_rule {
        rule       = { class = "kitty"       },
        properties = {
            focus     = true,
            floating  = true,
            placement = awful.placement.centered,
            width     = dpi(875),
            height    = dpi(500)
        }
    }

end)
