# Dotfiles - Raspberry Pi Version

This directory contains dotfiles that have been modified for use on Raspberry Pis.

# Compatibility

Dotfiles are mainly tested on a Raspberry Pi 4B, but should also work on other models.
