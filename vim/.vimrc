" ----------------------------------------------------------------------------
" Michael's .vimrc
"
" Help Pages
" http://cs.oberlin.edu/~kuperman/help/vim/home.html
" -----------------------------------------------------------------------------

" Allow Vim incompatibility with vi (make vim modern)
set nocompatible

" Enable plugins
filetype plugin on

" Enable menu for :find
set wildmenu

" Set row numbers on
set number

" Set syntax highlighting on
syntax on

" Tabs to spaces (4 spaces)
set expandtab
set tabstop=4
set shiftwidth=4 

" Set indentation on
set autoindent
set smartindent
" set cindent  " stricter rules for C programs

" Set folding method and automate saving/loading of views
set foldmethod=indent
set viewoptions-=options  " Prevent Vim from including working directory in views
" autocmd BufWinLeave *.* mkview
" autocmd BufWinEnter *.* silent loadview

" Set relative numbering on
set relativenumber

" Show commands
" set showcmd

" Create 'tags' file for source code
" Navigate with ^] for function under cursor
" Requires 'universal-ctags' or 'exuberant-ctags'
command! Maketags !ctags -R .  

" File specific commands
" autocmd FileType CMakeList set noexpandtab
autocmd FileType make setlocal noexpandtab

" Define highlight group for match
" Use ':mat[ch] find /<pattern>/' for search highlights
highlight find ctermbg=green guibg=green

""""""""""""""""""""""
" Keybinds
""""""""""""""""""""""
" Date & Time Binds
inoremap <Leader>DD <ESC>"=strftime('%A %d %b %Y')<CR>PEi
inoremap <Leader>dd <ESC>"=strftime('%R %Z')<CR>PEi

iab <expr> @date@ strftime('%A %d %b %Y')
iab <expr> @time@ strftime('%R %Z')

" Toggle spellcheck
nnoremap <Leader>s :set invspell<CR>
" Sets dictionary for spellcheck
set dictionary=/usr/share/dict/words

" Set search pattern highlights
" https://vim.fandom.com/wiki/Highlight_all_search_pattern_matches
set hlsearch
" Press Space to turn off highlighting and clear any message already displayed.
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>


" SNIPPETS:
" nnoremap <hotkey> <cmd>
" Note - <CR> is equavalent to pressing 'enter'
"
" Look into 'vim abbreviations' to autotype methods/functions/etc.
"
" 
" #####################################
"               Notes
" #####################################
"
" Autocomplete:
"       ^n/^p for default autocompletion (use anything vim can find)
"       ^x^f for filenames
"       ^x^n for local file-specific autocomplete
"       ^x^] for tags
"
" Spellcheck:
"       Turn on spellcheck with `set spell`
"       Navigate through spelling errors with `[s` and `]s`
"       Navigate through grammar errors with `[S` and `]S`
"
" ####################################
"             Plugins
" ####################################
"
" Setup Vim-Plug if necessary
if empty(glob('~/.vim/autoload/plug.vim'))
      silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
            autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
            endif


" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')


" ALE:
Plug 'dense-analysis/ale'


" VimWiki:
Plug 'vimwiki/vimwiki'
let g:vimwiki_folding = 'list'
" let g:vimwiki_list = [{'path': '~/vimwiki', 'auto_toc': 1, 'auto_diary_index': 1, 'exclude_files': ['**/README.md']}
"                      \]
let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': 'md', 'auto_toc': 1, 'auto_diary_index': 1, 'exclude_files': ['**/README.md']}]

" Colorizer:
Plug 'chrisbra/Colorizer'

" Tex-Conceal
Plug 'KeitaNakamura/tex-conceal.vim', {'for': 'tex'}  " Replaced 'for': 'tex'
set conceallevel=2
let g:tex_conceal="abdgm"
let g:tex_conceal_frac=1

" nnn file manager
Plug 'mcchrish/nnn.vim'
" Fixes required for v2.0 release
let g:nnn#set_default_mappings = 0  " Disable default mappings
nnoremap <silent> <leader>n :NnnPicker %:p:h<CR>
"~/<CR>
" '%:p:h'<CR>

" Lightline
Plug 'itchyny/lightline.vim'
set laststatus=2
set noshowmode

" Vim Indent Guides
Plug 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup = 1

" Vim Pandoc Syntax 
Plug 'vim-pandoc/vim-pandoc-syntax'
augroup pandoc_syntax
    autocmd! FileType vimwiki set syntax=markdown.pandoc
augroup END
let g:pandoc#syntax#conceal#urls = 1

" GOYO
Plug 'junegunn/goyo.vim'
nnoremap <silent> <leader>g :Goyo<CR>

" FZF
Plug 'junegunn/fzf'

"           Colour Schemes
" ####################################
Plug 'junegunn/seoul256.vim'
let g:seoul256_background = 238

" End of plugins
call plug#end()


" vim hardcodes background color erase even if the terminfo file does
" not contain bce (not to mention that libvte based terminals
" incorrectly contain bce in their terminfo files). This causes
" incorrect background rendering when using a color theme with a
" background color.
let &t_ut=''

" Set color scheme
" color elflord
color seoul256
