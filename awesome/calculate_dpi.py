#!/bin/python3
width = float(input("Enter physical screen width (cm): "))
height = float(input("Enter physical screen height (cm): "))

pix_width = float(input("Enter pixel width: "))
pix_height = float(input("Enter pixel height: "))

wh = (width**2 + height**2)**(1/2) / 2.54
p_wh = (pix_width**2 + pix_height**2)**(1/2)

print(f"DPI: {int(p_wh//wh)}")
