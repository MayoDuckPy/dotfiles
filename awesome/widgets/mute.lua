local awful = require("awful")
local wibox = require("wibox")

-- If (pacmd list-sources | awk '/muted: yes/' == "\tmuted: yes") then show mute icon
local muted

-- FIXME: Function will not work if other inputs are muted
local function is_mute()
    awful.spawn.easy_async_with_shell("pacmd list-sources | awk '/muted: yes/'", function(output)
        muted = (output == "\tmuted: yes")
    end)
end
