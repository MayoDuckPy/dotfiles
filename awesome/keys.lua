local awful = require("awful")
local beautiful = require("beautiful")
local naughty = require("naughty")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup")
local menubar = require("menubar")

-- Get dpi function from xresources
local dpi = require("beautiful.xresources").apply_dpi

local keys = {}

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
    -- However, you can use another modifier like Mod1, but it may interact with others.
    modkey = "Mod4"

    keys.globalkeys = gears.table.join(
        awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
                  {description="show help", group="awesome"}),
        awful.key({ modkey,           }, "h",   awful.tag.viewprev,
                  {description = "view previous", group = "tag"}),
        awful.key({ modkey,           }, "l",  awful.tag.viewnext,
                  {description = "view next", group = "tag"}),
        awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
                  {description = "go back", group = "tag"}),

        awful.key({ modkey,           }, "j",
            function ()
                awful.client.focus.byidx( -1)
            end,
            {description = "focus previous by index", group = "client"}
        ),
        awful.key({ modkey,           }, "k",
            function ()
                awful.client.focus.byidx(1)
            end,
            {description = "focus next by index", group = "client"}
        ),
        awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
                  {description = "show main menu", group = "awesome"}),

        -- Layout manipulation
        awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx( -1)    end,
                  {description = "swap with previous client by index", group = "client"}),
        awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx(  1)    end,
                  {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "l", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "h", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "Right",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "Left",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    --awful.key({ modkey, "Shift"   }, "j",     function () awful.tag.incnmaster( 1, nil, true) end,
    --          {description = "increase the number of master clients", group = "layout"}),
    --awful.key({ modkey, "Shift"   }, "k",     function () awful.tag.incnmaster(-1, nil, true) end,
    --          {description = "decrease the number of master clients", group = "layout"}),
    --awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
    --          {description = "increase the number of columns", group = "layout"}),
    --awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
    --          {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Run picom compositor
    awful.key({ modkey, "Shift" }, "p", function() awful.spawn.with_shell("picom --experimental-backends") end,
              {description = "open Python shell", group = "launcher"}),

    -- Screenshot a region
    awful.key({ modkey, "Shift" }, "s", function () 
            -- Sleep needed to fix input grabbing issue.
            -- Source: https://bbs.archlinux.org/viewtopic.php?id=224330
            local command = "sleep 0.2; scrot -sf '%Y-%m-%d_%H-%M-%S.png' -e 'mv $f ~/Pictures/Screenshots/'"
            awful.spawn.easy_async_with_shell(command, function() end)
        end,
        {description="show help", group="screen"}),

    -- Screenshot current window
    awful.key({ modkey, "Control" }, "s", function () 
            local command = "scrot -u '%Y-%m-%d_%H-%M-%S.png' -e 'mv $f ~/Pictures/Screenshots/'"
            awful.spawn.with_shell(command)
        end,
        {description="show help", group="screen"}),

    -- File explorer
    awful.key({ modkey, "Control" }, "Return", function () awful.spawn.with_shell("kitty nnn") end,
              {description = "open a nnn", group = "launcher"}),

    -- Prompt
    awful.key({ modkey, "Shift"},            "r",     function () awful.spawn.with_shell("rofi -show combi -mode combi") end,
              {description = "run prompt", group = "launcher"}),

    -- Menubar
    awful.key({ modkey }, "r", function() awful.spawn.with_shell("rofi -show drun") end,
              {description = "search list of applications", group = "launcher"}),

    -- Python shell
    -- Alternatively, modify this to launch a script
    awful.key({ modkey }, "p", function() awful.spawn.with_shell("kitty python3") end,
              {description = "open Python shell", group = "launcher"}),

    -- Lua code prompt
    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    
    -- Audio-related keybinds
    -- Volume function keys found using 'xmodmap -pke'
    awful.key({ }, "XF86AudioMute", function() 
        awful.spawn.with_shell("pactl set-sink-mute @DEFAULT_SINK@ toggle") 
    end, {description = "Toggle output mute", group = "audio"}),

    awful.key({ }, "XF86AudioLowerVolume", function() awful.spawn.with_shell("amixer -D pulse sset Master 5%-") end,
            {description = "Lower volume by 5%", group = "audio"}),

    awful.key({ }, "XF86AudioRaiseVolume", function() awful.spawn.with_shell("amixer -D pulse sset Master 5%+") end,
            {description = "Raise volume by 5%", group = "audio"}),

    awful.key({ modkey }, "`", function() 
            awful.spawn.with_shell("pactl set-source-mute @DEFAULT_SOURCE@ toggle")
            --  naughty.notify({
            --      title          = "Mute Mic", 
            --      text           = "Toggled Mute", 
            --      timeout        = 1, 
            --      width          = dpi(400), 
            --      height         = dpi(150), 
            --      position       = "top_middle",
            --      ignore_suspend = true
            --  }) 
        end, {description = "Toggle mic mute", group = "audio"}),

    awful.key({ }, "XF86AudioPlay", function() awful.spawn.with_shell("mpc toggle") end,
            {description = "Toggle playback", group = "audio"}),

    awful.key({ }, "XF86AudioPrev", function() awful.spawn.with_shell("mpc prev") end,
            {description = "Previous", group = "audio"}),

    awful.key({ }, "XF86AudioNext", function() awful.spawn.with_shell("mpc next") end,
            {description = "Next", group = "audio"}),


    awful.key({ modkey,           }, "Down",   function () awful.spawn.with_shell("picom-trans -c -5") end,
              {description = "decrease client opacity", group = "layout"}),
    awful.key({ modkey,           }, "Up",     function () awful.spawn.with_shell("picom-trans -c +5") end,
              {description = "increase client opacity", group = "layout"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
local ntags = 9
local tag_keys = {}
for i = 1, ntags do
    tag_keys = gears.table.join(tag_keys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end
keys.globalkeys = gears.table.join(keys.globalkeys, tag_keys)

keys.clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

keys.clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

keys.mousebuttons = gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
)

-- Set root (desktop) keys
root.keys(keys.globalkeys)
root.buttons(keys.mousebuttons)

return keys
