-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")


-- Imports
---------------------------------------------------------------------------------
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- MPD control library
--local mpc = require("mpc")

-- Widgets
--require("volume")
--require("mpd_current")
local mic_widget_builder = require("widgets.awmic")
local mic_widget = mic_widget_builder({font="FiraCode" .. " " .. "12"})

-- Theme handling library
local beautiful = require("beautiful")

-- DPI library
local xresources = beautiful.xresources
local dpi = xresources.apply_dpi

-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Load Debian menu entries
local debian = require("debian.menu")
local has_fdo, freedesktop = pcall(require, "freedesktop")

-- Import keybinds & rules
local keys = require("keys")
local rules = require("rules")


-- HiDPI Settings (HiDPI + LoDPI)
---------------------------------------------------------------------------------
-- Set Awesome to scale equally for HiDPI
-- This function helps to create a normalized desktop with the help of
-- "Xft.dpi: 192" in .Xresources and a ~2 scaling factor for the lower DPI display
-- Optionally, you may want to add "Xft.antialias: 1"
awful.screen.set_auto_dpi_enabled( true )


-- Error handling
---------------------------------------------------------------------------------
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end


-- Variable definitions
---------------------------------------------------------------------------------
-- Themes define colours, icons, font and wallpapers.
-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
local HOME = os.getenv("HOME")
local themes_dir = "themes"
local theme_name = "starter"
local theme_file = 'theme.lua'
beautiful.init(gears.filesystem.get_configuration_dir() .. themes_dir .. "/" .. theme_name .. "/" .. theme_file)

-- This is used later as the default terminal and editor to run.
terminal = "kitty --title Kitty" or "x-terminal-emulator"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor


-- Table of layouts to cover with awful.layout.inc, order matters.
-- Top layout defines default.
awful.layout.layouts = {
    awful.layout.suit.tile,
    -- awful.layout.suit.tile.left,
    awful.layout.suit.fair,
    -- awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.max,
    -- awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
    awful.layout.suit.floating,
}


-- Menu
---------------------------------------------------------------------------------
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

local menu_awesome = { "awesome", myawesomemenu, beautiful.awesome_icon }
local menu_terminal = { "open terminal", terminal }

if has_fdo then
    mymainmenu = freedesktop.menu.build({
        before = { menu_awesome },
        after =  { menu_terminal }
    })
else
    mymainmenu = awful.menu({
        items = {
                  menu_awesome,
                  { "Debian", debian.menu.Debian_menu.Debian },
                  menu_terminal,
                }
    })
end


-- mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
--                                      menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
-- mykeyboardlayout = awful.widget.keyboardlayout()


-- Wibar & Widgets
---------------------------------------------------------------------------------
-- Create a textclock widget
mytextclock = wibox.widget.textclock()
mytextclock.format = "%A, %b. %d | %r"  -- Time strings can be found in `man strftime`
mytextclock.refresh = 1

-- Widget that checks whether mic is muted
--mic_check = awful.widget.watch('bash -c "pacmd list-sources | awk \'/muted: yes/\' | sed \'s/\tm/M/\'"', 1,
--    function(widget, output)
--        if output ~= "" then
--            widget:set_text("Muted: yes ")
--        else
--            widget:set_text("Muted: no ")
--        end
--    end
--)

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),

    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),

    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = gears.table.join(
    awful.button({ }, 1, function (c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal(
                "request::activate",
                "tasklist",
                {raise = true}
            )
        end
    end),

    awful.button({ }, 3, function()
        awful.menu.client_list({ theme = { width = 250 } })
    end),

    awful.button({ }, 4, function ()
        awful.client.focus.byidx(1)
    end),

    awful.button({ }, 5, function ()
        awful.client.focus.byidx(-1)
    end))

local function set_wallpaper(s)
    -- Call 'feh' to set wallpaper
    awful.spawn.with_shell("exec ~/.fehbg")

    -- -- Wallpaper
    -- if beautiful.wallpaper then
    --     local wallpaper = beautiful.wallpaper
    --     -- If wallpaper is a function, call it with the screen
    --     if type(wallpaper) == "function" then
    --         wallpaper = wallpaper(s)
    --     end
    --     gears.wallpaper.maximized(wallpaper, s, true)
    -- end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- Sets the taskbar for each screen
awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.noempty,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.minimizedcurrenttags,
        buttons = tasklist_buttons,
        style   = {
            shape_border_width = dpi(1),
            shape_border_color = "#ededed",
            --shape              = gears.shape.rounded_bar,
        },
        layout  = {
            spacing = dpi(10),
            --spacing_widget = {
            --    {
            --        forced_width = dpi(5),
            --        shape        = gears.shape.circle,
            --        widget       = wibox.widget.separator  -- Adds dot between clients
            --    },
            --    valign = 'center',
            --    halign = 'center',
            --    widget = wibox.container.place,
            --},
            layout = wibox.layout.flex.horizontal
        },
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        expand = "none",
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            -- mylauncher,
            s.mylayoutbox,
            s.mytaglist,
            --s.mypromptbox,
            mic_widget,
            s.mytasklist,
        },

        {
        -- Middle widget
        layout = wibox.layout.fixed.horizontal,
        mytextclock,
        },

        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            --mykeyboardlayout,
            --volume_widget,
            --mpd_widget,
            --mic_check,
            wibox.widget.systray(true),  -- true reverses order
            --awful.widget.only_on_screen(wibox.widget.systray(), 2),
        }
    }
end)


-- Notifications
---------------------------------------------------------------------------------
naughty.config.presets = gears.table.join(naughty.config.presets,
    {
        spacing = dpi(3)
    }
)


-- Rules
---------------------------------------------------------------------------------
-- Rules to apply to new clients (through the "manage" signal).
-- Rules can be found in rules.lua
awful.rules.rules = rules


-- Signals
---------------------------------------------------------------------------------
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
 
    -- Enable rounded corners by default
    c.shape = function(cr,w,h) gears.shape.rounded_rect(cr, w, h, 10) end  -- Radius of 5px
end)

-- Make clients have rounded corners except when they are maximized/fullscreen
-- Note: Reshaping has a slight performance penalty
client.connect_signal("property::size", function(c)
    -- TODO: Add a table of client exceptions
    -- i.e. if c.maximized or c.fullscreen or c in exceptions then ...
    if c.maximized or c.fullscreen then
        c.shape = function(c, w, h) gears.shape.rectangle(c, w, h) end
    else
        -- Enable rounded corners
        c.shape = function(cr,w,h) gears.shape.rounded_rect(cr, w, h, 10) end  -- Radius of 5px
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
          --awful.titlebar.widget.floatingbutton (c),
          --awful.titlebar.widget.stickybutton   (c),
          --awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)


-- Autostart
---------------------------------------------------------------------------------
autostart = true
if autostart then
    awful.spawn.with_shell("~/.autostart.sh")
end

-- Start compositor
awful.spawn.with_shell("picom --experimental-backends")
