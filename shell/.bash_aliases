alias python=python3
alias pip=pip3
alias pdb="python3 -m pdb"
alias pydoc="python3 -m pydoc"
alias new-project="mkdir src && mkdir build"
alias update="sudo apt update && apt list --upgradeable"
alias firefoxp="firefox --private-window"
alias picom="picom --experimental-backends"
alias icat="kitty +kitten icat --transfer-mode=stream --stdin=yes"

if [ -d ~/Apps/gpodder ]; then
    alias gpodder="~/Apps/gpodder/bin/gpodder"
fi

# Use alternatives commands if available
#if command -v bat &> /dev/null; then
#    alias cat=bat
#fi
if command -v exa &> /dev/null; then
    alias ls=exa
fi
# if command -v fd &> /dev/null;
# then
#     alias find=fd
# fi
if command -v nvim &> /dev/null; then
    alias vim=nvim
fi
# if command -v procs &> /dev/null;
# then
#     alias ps=procs
# fi
if command -v rg &> /dev/null; then
    alias grep=rg
fi
