-- awesome_mode: api-level=4:screen=on
-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")


-- Imports
---------------------------------------------------------------------------------
-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
-- TODO: Widgets missing style
-- TODO: Widget box with switches (mouse accel., etc.)
local beautiful = require("beautiful")

-- DPI Library
local xresources = beautiful.xresources
local dpi = xresources.apply_dpi

-- Notification library
local naughty = require("naughty")

-- Declarative object management
local ruled = require("ruled")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Widgets
local mic_widget = require("widgets.awmic")
local mic_widget = mic_widget({font="Fira Code" .. " " .. "12"})

-- Import keybinds & rules
require("keys")
require("rules")


-- HiDPI Settings (HiDPI + LoDPI)
---------------------------------------------------------------------------------
-- Set Awesome to scale equally for HiDPI
-- This function helps to create a normalized desktop with the help of
-- "Xft.dpi: 192" in .Xresources and a ~2 scaling factor for the lower DPI display
-- Optionally, you may want to add "Xft.antialias: 1"
awful.screen.set_auto_dpi_enabled(true)


-- Error Handling
---------------------------------------------------------------------------------
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
naughty.connect_signal("request::display_error", function(message, startup)
    naughty.notification {
        urgency = "critical",
        title   = "Oops, an error happened"..(startup and " during startup!" or "!"),
        message = message
    }
end)


-- Variable Definitions
---------------------------------------------------------------------------------
-- Themes define colours, icons, font and wallpapers.
-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
local HOME = os.getenv("HOME")
local themes_dir = "themes"
local theme_name = "starter"
local theme_file = 'theme.lua'
beautiful.init(gears.filesystem.get_configuration_dir() .. themes_dir .. "/" .. theme_name .. "/" .. theme_file)

-- This is used later as the default terminal and editor to run.
terminal = "kitty"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"


-- Menu
---------------------------------------------------------------------------------
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "open terminal", terminal }
                                  }
                        })

-- mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
--                                      menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it


-- Layouts
---------------------------------------------------------------------------------
-- Table of layouts to cover with awful.layout.inc, order matters.
tag.connect_signal("request::default_layouts", function()
    awful.layout.append_default_layouts({
        awful.layout.suit.fair,
        awful.layout.suit.tile,
        -- awful.layout.suit.tile.left,
        -- awful.layout.suit.tile.bottom,
        awful.layout.suit.tile.top,
        awful.layout.suit.fair.horizontal,
        awful.layout.suit.spiral,
        -- awful.layout.suit.spiral.dwindle,
        -- awful.layout.suit.max,
        -- awful.layout.suit.max.fullscreen,
        -- awful.layout.suit.magnifier,
        -- awful.layout.suit.corner.nw,
        awful.layout.suit.floating,
    })
end)


-- Wallpaper
---------------------------------------------------------------------------------

local function set_wallpaper(s)
    -- Call 'feh' to set wallpaper
    awful.spawn.with_shell("exec ~/.fehbg")

    -- if beautiful.wallpaper then
    --     local wallpaper = beautiful.wallpaper
    --     -- If wallpaper is a function, call it with the screen
    --     if type(wallpaper) == "function" then
    --         wallpaper = wallpaper(s)
    --     end
    --     gears.wallpaper.maximized(wallpaper, s, true)
    -- end
end

screen.connect_signal("request::wallpaper", function(s)
    set_wallpaper(s)
end)

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)


-- Wibar & Widgets
---------------------------------------------------------------------------------

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- Create a textclock widget
mytextclock = wibox.widget.textclock()
mytextclock.format = "%A, %b. %d | %r"  -- Time strings can be found in `man strftime`
mytextclock.refresh = 1
mytextclock.font = beautiful.textclock_font


screen.connect_signal("request::desktop_decoration", function(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox {
        screen  = s,
        buttons = {
            awful.button({ }, 1, function () awful.layout.inc( 1) end),
            awful.button({ }, 3, function () awful.layout.inc(-1) end),
            awful.button({ }, 4, function () awful.layout.inc(-1) end),
            awful.button({ }, 5, function () awful.layout.inc( 1) end),
        }
    }

-- Wibar Widgets
--------------------
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.noempty,
        buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ modkey }, 1, function(t)
                                            if client.focus then
                                                client.focus:move_to_tag(t)
                                            end
                                        end),
            awful.button({ }, 3, awful.tag.viewtoggle),
            awful.button({ modkey }, 3, function(t)
                                            if client.focus then
                                                client.focus:toggle_tag(t)
                                            end
                                        end),
            awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
            awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end),
        }
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        -- FIXME: Will show nothing if client has no icon
        filter  = awful.widget.tasklist.filter.minimizedcurrenttags,
        buttons = {
            awful.button({ }, 1, function (c)
                c:activate { context = "tasklist", action = "toggle_minimization" }
            end),
            awful.button({ }, 3, function() awful.menu.client_list { theme = { width = 250 } } end),
            awful.button({ }, 4, function() awful.client.focus.byidx(-1) end),
            awful.button({ }, 5, function() awful.client.focus.byidx( 1) end),
        }
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Create systray
    systray = wibox.widget.systray()
    systray.reverse = true
    systray.opacity = beautiful.systray_opacity  -- Opacity does not work

    -- Add widgets to the wibox
    s.mywibox.widget = {
        layout = wibox.layout.align.horizontal,
        expand = "none",
        { -- Left widgets
            s.mylayoutbox,
            layout = wibox.layout.fixed.horizontal,
            -- mylauncher,
            s.mytaglist,
            -- s.mypromptbox,
            mic_widget,
            s.mytasklist,
        },
        {
            -- Middle widget
            layout = wibox.layout.fixed.horizontal,
            mytextclock,
        },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            systray
            -- mykeyboardlayout,
        },
    }
end)


-- Titlebars
---------------------------------------------------------------------------------
-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = {
        awful.button({ }, 1, function()
            c:activate { context = "titlebar", action = "mouse_move"  }
        end),
        awful.button({ }, 3, function()
            c:activate { context = "titlebar", action = "mouse_resize"}
        end),
    }

    titlewidget = awful.titlebar.widget.titlewidget(c)
    titlewidget.font = beautiful.titlebar_font
    awful.titlebar(c, {size=beautiful.titlebar_size}).widget = {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = titlewidget
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            -- awful.titlebar.widget.floatingbutton (c),
            -- awful.titlebar.widget.maximizedbutton(c),
            -- awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)


-- Signals
---------------------------------------------------------------------------------
-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:activate { context = "mouse_enter", raise = false }
end)

-- Enables rounded edges on clients
client.connect_signal('request::manage', function(c)
    -- Add a border around the client
    c.border_width = beautiful.border_width

    -- If client not maximized and not fullscreen, make it rounded
    if not (c.maximized or c.fullscreen) then
        -- Round client corners
        c.shape = function(cr, w, h)
            gears.shape.rounded_rect(cr, w, h, beautiful.border_radius)
        end
    end
end)

-- Make clients have rounded corners except when they are maximized/fullscreen
-- Note: Reshaping has a slight performance penalty
client.connect_signal("property::size", function(c)
    -- TODO: Add a table of client exceptions
    -- i.e. if c.maximized or c.fullscreen or c in exceptions then ...
    if c.maximized or c.fullscreen and not (c.maximized and c.fullscreen) then
        c.shape = function(c, w, h) gears.shape.rectangle(c, w, h) end
    else
        -- Enable rounded corners
        c.shape = function(cr,w,h)
            gears.shape.rounded_rect(cr, w, h, beautiful.border_radius)
        end

        -- Fixes when fullscreen and maximize overlap and border_width resets
        -- NOTE: Maximize does not properly calculate client width if border_width > 0
        --       Otherwise, we could define border_width when shaping client as rect.
        -- NOTE: A border for a maximized and fullscreen client is only needed for multiple screens.
        -- TODO: Add something to differentiate focused screens
        --       (e.g. gradient animation around screen border when switching)
        c.border_width = beautiful.border_width
    end
end)

-- Show border on focused client
client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)


-- Notifications
---------------------------------------------------------------------------------

ruled.notification.connect_signal('request::rules', function()
    -- All notifications will match this rule.
    ruled.notification.append_rule {
        rule       = { },
        properties = {
            screen           = awful.screen.preferred,
            implicit_timeout = 5,
        }
    }
end)

naughty.connect_signal("request::display", function(n)
    naughty.layout.box { notification = n }
end)


-- Autostart
---------------------------------------------------------------------------------
autostart = true
if autostart then
    awful.spawn.with_shell("~/.autostart.sh")
end

-- Start compositor
awful.spawn.with_shell("picom --experimental-backends")
