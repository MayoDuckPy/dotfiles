local awful = require("awful")
local beautiful = require("beautiful")
local naughty = require("naughty")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup")
local menubar = require("menubar")

-- Get dpi function from xresources
local dpi = require("beautiful.xresources").apply_dpi

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
    -- However, you can use another modifier like Mod1, but it may interact with others.
    modkey = "Mod4"
    shift  = "Shift"
    ctrl   = "Control"
    alt    = "Mod1"

awful.keyboard.append_global_keybindings({
    -- General Awesome keys
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),
    awful.key({ modkey, ctrl }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),
    -- Lua code prompt
    -- awful.key({ modkey }, "x",
    --           function ()
    --               awful.prompt.run {
    --                 prompt       = "Run Lua code: ",
    --                 textbox      = awful.screen.focused().mypromptbox.widget,
    --                 exe_callback = awful.util.eval,
    --                 history_path = awful.util.get_cache_dir() .. "/history_eval"
    --               }
    --           end,
    --           {description = "lua execute prompt", group = "awesome"}),

    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "awesome"}),
})

awful.keyboard.append_global_keybindings({
    -- Tags related keybindings
    awful.key({ modkey, ctrl      }, "h",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey, ctrl      }, "l",   awful.tag.viewnext,
              {description = "view next",     group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),
})

awful.keyboard.append_global_keybindings({
    -- Focus bindings
    -- TODO: May want to move focusing and swapping code in some lib
    --       function because repetition makes changing things annoying.
    awful.key({ modkey,           }, "j",
        function() 
            awful.client.focus.bydirection("down") 
            if client.focus then client.focus:raise() end
        end,
        {description = "focus client below", group = "client"}),
    awful.key({ modkey,           }, "k",
        function()
            awful.client.focus.bydirection("up")    
            if client.focus then client.focus:raise() end
        end,
        {description = "focus client above", group = "client"}),
    awful.key({ modkey,           }, "h", 
        function()
            awful.client.focus.bydirection("left")  
            if client.focus then client.focus:raise() end
        end,
        {description = "focus client to the left", group = "client"}),
    awful.key({ modkey,           }, "l",
        function()
            awful.client.focus.bydirection("right") 
            if client.focus then client.focus:raise() end
        end,
        {description = "focus client to the right", group = "client"}),
    awful.key({ modkey, alt }, "l", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, alt }, "h", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",  -- Equivalent to Windows 'alt-tab'
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),
    awful.key({ modkey, ctrl }, "n",
        function ()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:activate { raise = true, context = "key.unminimize" }
            end
        end,
        {description = "restore minimized", group = "client"}),
})

awful.keyboard.append_global_keybindings({
    -- Layout manipulation
    awful.key({ modkey, shift   }, "j", function () awful.client.swap.bydirection("down")  end,
              {description = "swap with client below", group = "client"}),
    awful.key({ modkey, shift   }, "k", function () awful.client.swap.bydirection("up")    end,
              {description = "swap with client above", group = "client"}),
    awful.key({ modkey, shift   }, "h", function () awful.client.swap.bydirection("left")  end,
              {description = "swap with client to the left", group = "client"}),
    awful.key({ modkey, shift   }, "l", function () awful.client.swap.bydirection("right") end,
              {description = "swap with client to the right", group = "client"}),
    awful.key({ modkey,           }, "Right",    function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "Left",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    --awful.key({ modkey, shift   }, "j",     function () awful.tag.incnmaster( 1, nil, true) end,
    --          {description = "increase the number of master clients", group = "layout"}),
    --awful.key({ modkey, shift   }, "k",     function () awful.tag.incnmaster(-1, nil, true) end,
    --          {description = "decrease the number of master clients", group = "layout"}),
    --awful.key({ modkey, ctrl }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
    --          {description = "increase the number of columns", group = "layout"}),
    --awful.key({ modkey, ctrl }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
    --          {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1) end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, shift     }, "space", function () awful.layout.inc(-1) end,
              {description = "select previous", group = "layout"}),

    -- Requires 'picom'
    awful.key({ modkey,           }, "Down",   function () awful.spawn.with_shell("picom-trans -c -5") end,
              {description = "decrease client opacity", group = "layout"}),
    awful.key({ modkey,           }, "Up",     function () awful.spawn.with_shell("picom-trans -c +5") end,
              {description = "increase client opacity", group = "layout"})
})

awful.keyboard.append_global_keybindings({
    -- Screenshot keybinds
    -- Screenshot a region
    awful.key({ modkey, shift }, "s", function () 
            -- Sleep needed to fix input grabbing issue.
            -- Source: https://bbs.archlinux.org/viewtopic.php?id=224330
            local command = "sleep 0.2; scrot -sf '%Y-%m-%d_%H-%M-%S.png' -e 'xclip -i $f -t image/png -selection clipboard && mv $f ~/Pictures/Screenshots/'"
            awful.spawn.easy_async_with_shell(command, function() end)
        end,
        {description="show help", group="screenshot"}),

    -- Screenshot current window
    awful.key({ modkey, ctrl }, "s", function () 
            local command = "scrot -u '%Y-%m-%d_%H-%M-%S.png' -e 'xclip -i $f -t image/png -selection clipboard && mv $f ~/Pictures/Screenshots/'"
            awful.spawn.with_shell(command)
        end,
        {description="show help", group="screenshot"}),
})

awful.keyboard.append_global_keybindings({
    -- Launcher-related keybinds
    -- File explorer
    -- TODO: Use default term variable instead of hardcoding
    -- Requires 'kitty' and 'nnn'
    awful.key({ modkey, ctrl }, "Return", function () awful.spawn.with_shell("kitty nnn") end,
              {description = "open a nnn", group = "launcher"}),

    -- Prompt
    -- Requires 'rofi'
    awful.key({ modkey, shift},            "r",     function () awful.spawn.with_shell("rofi -show combi -mode combi") end,
              {description = "run prompt", group = "launcher"}),

    -- Menubar
    awful.key({ modkey }, "r", function() awful.spawn.with_shell("rofi -show drun") end,
              {description = "search list of applications", group = "launcher"}),

    -- Python shell
    -- Alternatively, modify this to launch a script
    -- TODO: Use default term variable instead of hardcoding
    -- Requires 'python3'
    awful.key({ modkey }, "p", function() awful.spawn.with_shell("kitty python3") end,
              {description = "open Python shell", group = "launcher"}),

    -- Run picom compositor
    awful.key({ modkey, shift }, "p", function() awful.spawn.with_shell("picom --experimental-backends") end,
              {description = "open Python shell", group = "launcher"}),
})


awful.keyboard.append_global_keybindings({
    -- Audio-related keybinds
    -- Volume function keys found using 'xmodmap -pke'
    awful.key({ }, "XF86AudioMute", function() 
        awful.spawn.with_shell("pactl set-sink-mute @DEFAULT_SINK@ toggle") 
    end,
    {description = "Toggle output mute", group = "audio"}),

    awful.key({ }, "XF86AudioLowerVolume", function()
        awful.spawn.with_shell("amixer -D pulse sset Master 5%-")
    end,
    {description = "Lower volume by 5%", group = "audio"}),

    awful.key({ }, "XF86AudioRaiseVolume", function()
        awful.spawn.with_shell("amixer -D pulse sset Master 5%+")
    end,
    {description = "Raise volume by 5%", group = "audio"}),

    awful.key({ modkey }, "`", function() 
            awful.spawn.with_shell("pactl set-source-mute @DEFAULT_SOURCE@ toggle")
            --  naughty.notify({
            --      title          = "Mute Mic", 
            --      text           = "Toggled Mute", 
            --      timeout        = 1, 
            --      width          = dpi(400), 
            --      height         = dpi(150), 
            --      position       = "top_middle",
            --      ignore_suspend = true
            --  }) 
        end, {description = "Toggle mic mute", group = "audio"}),

    awful.key({ }, "XF86AudioPlay", function() awful.spawn.with_shell("mpc toggle") end,
            {description = "Toggle playback", group = "audio"}),

    awful.key({ }, "XF86AudioPrev", function() awful.spawn.with_shell("mpc prev") end,
            {description = "Previous", group = "audio"}),

    awful.key({ }, "XF86AudioNext", function() awful.spawn.with_shell("mpc next") end,
            {description = "Next", group = "audio"}),

    -- Picom transparency
    awful.key({ modkey,           }, "Down",   function () awful.spawn.with_shell("picom-trans -c -5") end,
              {description = "decrease client opacity", group = "layout"}),
    awful.key({ modkey,           }, "Up",     function () awful.spawn.with_shell("picom-trans -c +5") end,
              {description = "increase client opacity", group = "layout"})
})

-- Game Mode
--
-- Requires: picom, xwinwrap, and mpv.
-- Disables mouse acceleration, compositor, and dynamic wallpaper (if enabled)
-- to improve performance while gaming.
--
-- NOTE: Mouse acceleration keybinds should be separated if they have use
--       elsewhere.
-- TODO: Implement 'game mode' indicator widget.
awful.keyboard.append_global_keybindings({
    awful.key({ modkey,           }, "\\", function ()
        awful.spawn.with_shell("xset m 0 0; pkill picom; pkill xwinwrap") end,
              {description = "disable mouse acceleration", group = "hotkeys"}),
    awful.key({ modkey, ctrl      }, "\\", function ()
        awful.spawn.with_shell("xset m default; picom --experimental-backends")
        if beautiful.enable_dynamic_wallpaper then
            awful.spawn.easy_async_with_shell("exec ~/Scripts/dynamic-wallpapers.sh &", function() end)
        end
    end,      {description = "enable mouse acceleration", group = "hotkeys"})
})

awful.keyboard.append_global_keybindings({
    awful.key {
        modifiers   = { modkey },
        keygroup    = "numrow",
        description = "only view tag",
        group       = "tag",
        on_press    = function (index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                tag:view_only()
            end
        end,
    },
    awful.key {
        modifiers   = { modkey, ctrl },
        keygroup    = "numrow",
        description = "toggle tag",
        group       = "tag",
        on_press    = function (index)
            local screen = awful.screen.focused()
            local tag = screen.tags[index]
            if tag then
                awful.tag.viewtoggle(tag)
            end
        end,
    },
    awful.key {
        modifiers = { modkey, shift },
        keygroup    = "numrow",
        description = "move focused client to tag",
        group       = "tag",
        on_press    = function (index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:move_to_tag(tag)
                end
            end
        end,
    },
    awful.key {
        modifiers   = { modkey, ctrl, shift },
        keygroup    = "numrow",
        description = "toggle focused client on tag",
        group       = "tag",
        on_press    = function (index)
            if client.focus then
                local tag = client.focus.screen.tags[index]
                if tag then
                    client.focus:toggle_tag(tag)
                end
            end
        end,
    },
    awful.key {
        modifiers   = { modkey },
        keygroup    = "numpad",
        description = "select layout directly",
        group       = "layout",
        on_press    = function (index)
            local t = awful.screen.focused().selected_tag
            if t then
                t.layout = t.layouts[index] or t.layout
            end
        end,
    }
})

client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({
        awful.key({ modkey,           }, "f",
            function (c)
                c.fullscreen = not c.fullscreen
                c:raise()
            end,
            {description = "toggle fullscreen", group = "client"}),
        awful.key({ modkey, shift   }, "c",      function (c) c:kill()                         end,
                {description = "close", group = "client"}),
        awful.key({ modkey, ctrl }, "space",  awful.client.floating.toggle                     ,
                {description = "toggle floating", group = "client"}),
        awful.key({ modkey, ctrl }, "Return", function (c) c:swap(awful.client.getmaster()) end,
                {description = "move to master", group = "client"}),
        awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
                {description = "move to screen", group = "client"}),
        awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
                {description = "toggle keep on top", group = "client"}),
        awful.key({ modkey,           }, "n",
            function (c)
                -- The client currently has the input focus, so it cannot be
                -- minimized, since minimized clients can't have the focus.
                c.minimized = true
            end ,
            {description = "minimize", group = "client"}),
        awful.key({ modkey,           }, "m",
            function (c)
                c.maximized = not c.maximized
                c:raise()
            end ,
            {description = "(un)maximize", group = "client"}),
        awful.key({ modkey, ctrl }, "m",
            function (c)
                c.maximized_vertical = not c.maximized_vertical
                c:raise()
            end ,
            {description = "(un)maximize vertically", group = "client"}),
        awful.key({ modkey, shift   }, "m",
            function (c)
                c.maximized_horizontal = not c.maximized_horizontal
                c:raise()
            end ,
            {description = "(un)maximize horizontally", group = "client"}),
    })
end)

-- Mouse bindings
client.connect_signal("request::default_mousebindings", function()
    awful.mouse.append_client_mousebindings({
        awful.button({ }, 1, function (c)
            c:activate { context = "mouse_click" }
        end),
        awful.button({ modkey }, 1, function (c)
            c:activate { context = "mouse_click", action = "mouse_move"  }
        end),
        awful.button({ modkey }, 3, function (c)
            c:activate { context = "mouse_click", action = "mouse_resize"}
        end),
    })
end)

awful.mouse.append_global_mousebindings({
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
})
