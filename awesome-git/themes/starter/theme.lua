---------------------------
-- Simple awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local awful = require("awful")

local dpi = xresources.apply_dpi

local gears = require("gears")
local gfs = gears.filesystem
local themes_path = gfs.get_themes_dir()

local HOME = os.getenv("HOME")
local tmp_themes_path = HOME .. "/.config/awesome/themes/"
local theme_name = "starter"

local icon_dir = "/usr/share/icons/Papirus-Dark"
local titlebar_icons_dir = icon_dir .. "/symbolic/actions/"

local colorscheme = xresources.get_current_theme()
local theme = {}
local red = "#ff0000"
local sky_blue = "#66d4ff"
local gray = "#444444"
local dark_gray = "#171717"  --"#3b3b3b"
local charcoal = "#262626"
local onyx = "#0f0f0f"
local black = "#000308"
local bg_opacity = "ef"

local theme_font = "Iosevka "
local font_size = 12
theme.font = theme_font .. font_size
theme.maximized_hide_border = true


-- Background & Foreground Defaults
---------------------------------------------------------------------------------
theme.bg_normal     = onyx
theme.bg_focus      = theme.bg_normal
theme.bg_urgent     = sky_blue
theme.bg_minimize   = sky_blue  -- Also defines mod-key colors in keybind menu
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#ffffff"
theme.fg_focus      = theme.fg_normal
theme.fg_urgent     = theme.fg_normal
theme.fg_minimize   = theme.fg_normal

theme.useless_gap   = dpi(8)
theme.border_width  = dpi(1)
theme.border_normal = "#00000000"
theme.border_focus  = "#03fcfc"
theme.border_marked = "#91231c"

theme.enable_dynamic_wallpaper = false

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"



-- Notifications
---------------------------------------------------------------------------------
-- Variables set for theming notifications:
theme.notification_height = dpi(180)
theme.notification_width = dpi(425)
theme.notification_max_width = theme.notifcation_width
theme.notification_max_height = theme.notification_height
theme.notification_font = theme_font .. font_size
theme.notification_opacity = 85
theme.notification_shape = gears.shape.rounded_rect
--theme.notification_border_width = theme.notification_width
--theme.notification_shape = gears.shape.rounded_bar
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]


-- Menu Theme Variables
---------------------------------------------------------------------------------
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = titlebar_icons_dir .. "go-next-symbolic.svg"
theme.menu_height = dpi(25)
theme.menu_width  = dpi(150)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"


-- Tasklist (Wibar?) Theme Variables
---------------------------------------------------------------------------------
theme.tasklist_bg_minimize = theme.bg_normal
theme.tasklist_disable_task_name = true
theme.tasklist_spacing = dpi(0)
theme.tasklist_align = "center"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)

theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Systray
theme.systray_icon_spacing = dpi(5)
theme.systray_opacity = 0.94

theme.taglist_font = theme_font .. "Bold " .. font_size

-- Titlebar Theme Variables
---------------------------------------------------------------------------------

-- Titlebar Sizing
theme.titlebar_size = dpi(15)
theme.titlebar_font = theme_font .. font_size - 4

-- Titlebar Buttons & Icons
-- theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = titlebar_icons_dir .. "window-close-symbolic.svg"

theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = titlebar_icons_dir .. "window-minimize-symbolic.svg"

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"


-- Widget Variables
---------------------------------------------------------------------------------
theme.textclock_font = theme_font .. "Bold " .. font_size


-- Theme Wallpaper
---------------------------------------------------------------------------------
-- Set theme wallpaper (disabled in favour of 'feh')
-- theme.wallpaper = themes_path.."default/background.png"


-- Layout Icons
---------------------------------------------------------------------------------
-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = icon_dir .. "/32x32@2x/actions/" .. "home.svg"
--[[theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)
--]]

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "Papirus-Dark"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
