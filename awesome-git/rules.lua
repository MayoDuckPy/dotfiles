-- Load dependencies
local awful = require("awful")
local beautiful = require("beautiful")
local ruled = require("ruled")

-- Get dpi function from xresources
local dpi = require("beautiful.xresources").apply_dpi

-- Remove if issue #2497 is resolved (https://github.com/awesomeWM/awesome/issues/2497)
-----------------
-- Function from
-- https://github.com/alfunx/.dotfiles/blob/master/.config/awesome/config/rules.lua
--
-- placement, that should be applied after setting x/y/width/height/geometry
function awful.rules.delayed_properties.delayed_placement(c, value, props) --luacheck: no unused
    if props.delayed_placement then
        awful.rules.extra_properties.placement(c, props.delayed_placement, props)
    end
end

-- Rules to apply to new clients.
ruled.client.connect_signal("request::rules", function()
    -- All clients will match this rule.
    ruled.client.append_rule {
        id         = "global",
        rule       = { },
        properties = {
            focus     = awful.client.focus.filter,
            raise     = true,
            screen    = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    }

    -- Floating clients.
    ruled.client.append_rule {
        id       = "floating",
        rule_any = {
            instance = { "copyq", "pinentry" },
            class    = {
                "Arandr", "Blueman-manager", "Chiaki", "Gpick", "Kruler",
                "qemu", "QEMU", "Steam", "Tor Browser", "Wpa_gui", "veromix",
                "xtightvncviewer"
            },
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name    = {
                "Event Tester",  -- xev.
            },
            role    = {
                "AlarmWindow",    -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",         -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        except_any  = {
            name    = {
                "Steam",  -- Main Steam client window
            },
        },
        properties = { floating          = true,
                       delayed_placement = awful.placement.centered }
    }

    -- TODO: Add titlebars to normal clients and dialogs that are floating
    ruled.client.append_rule {
        id         = "titlebars",
        rule_any   = { type = { "normal", "dialog" } },
        properties = {
                        -- floating_titlebars = true,
                        titlebars_enabled = false
                     }
    }

    -- Floating and Focused
    ruled.client.append_rule {
        id         = "focused-floating",
        rule_any   = {
            class = {
                        "KeePassXC",
                        "Nextcloud",
                    },
        },
        properties = {
                        focus             = true,
                        floating          = true,
                        delayed_placement = awful.placement.centered
                     }
    }

    -- Titlebars Always Enabled
    ruled.client.append_rule {
        id         = "titlebars",
        rule_any   = {
            class = {
                "xournalpp", "com.github.xournalpp.xournalpp"
            }
        },
        properties = { titlebars_enabled = true }
    }

    -- Application-specific Rules
    ruled.client.append_rule {
        rule       = { class = "discord"     },
        properties = { screen = 2, tag = "1" }
    }

    ruled.client.append_rule {
        rule       = { class = "kitty"       },
        properties = {
            focus             = true,
            floating          = true,
            width             = dpi(875),
            height            = dpi(500),
            titlebars_enabled = false,
            delayed_placement = awful.placement.centered
        }
    }

    ruled.client.append_rule {
        rule       = { class = "Sxiv"       },
        properties = {
                        floating          = true,
                        focused           = true,
                        width             = dpi(720),
                        height            = dpi(405),
                        titlebars_enabled = false,
                        delayed_placement = awful.placement.centered
                     },
    }

    ruled.client.append_rule {
        rule       = { class = "Zathura"    },
        properties = {
                        delayed_placement = awful.placement.centered,
                        titlebars_enabled = false
                     }
    }

end)
